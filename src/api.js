const boolDP = process.env.NODE_ENV !== 'development'
/*
    @author : Suraj Chavan,
    @params : Accept default option {} , 
    @description : This function accepts a object option and return onother function,
                which again accepts object as option argument and combine both object 
                and return a fetch request promise
    @return : Return a function which accept another option
*/
const request = defaults => options => {
    options = Object.assign({},defaults, options)
    let isBlob = options.blob || false
    delete options.blob
    return fetch(options.url,options)
        .then(resp => {
            if (resp.status === 401) {
                boolDP ? (location.href = "/", sessionStorage.clear()) : ''
            }
            if(!isBlob)
                return resp.json()
            return resp.blob()
        })
}
// defaulting
export const createRequester = request({
    method: 'GET',
    headers: { 
        'Content-Type': 'application/json',
        'sessionid': sessionStorage.getItem("_ticket") 
    }
})
