import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import CreateView from "../views/CreateView.vue";
import CustomView from "../views/CustomView";
import TableView from "../views/TableView.vue";
import CardView from "../views/CardView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/createview",
    name: "CreateView",
    component: CreateView
  },
  {
    path: "/customview",
    name: "CustomView",
    component: CustomView
  },
  {
    path: "/view/:title/table",
    name: "table",
    component: TableView
  },
  {
    path: "/view/:title/card",
    name: "cards",
    component: CardView
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
