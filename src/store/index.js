import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    selectedView: "",
    viewData:[],
    columns:[],
    selectedColumns:[],
    title:"",
    cardTitleColumn:"",
    customColNames:{},
    displayType: "",
    cardType:"3col",
    dataOptions: [
      {id:"0", name: "Securities"},
      {id:"1", name: "Client Holdings"}
    ]
  },
  mutations: {
    setSelectedView: function(state, view) {
      state.selectedView = view;
      state.viewData = [];
      state.selectedColumns = [];
      state.cardTitleColumn = "";
      state.setCustomColNames = {};
      state.displayType = "";
      state.cardType = "3col";
    },
    setViewData: function(state, data) {
      state.viewData = data;
      state.columns = Object.keys(data[0]);
    },
    setSelectedColumns: function(state, selectedColumns) {
      state.selectedColumns = selectedColumns;
      if(selectedColumns.length > 12){
        state.displayType = "table";
      }
    },
    setTitle: function(state,title) {
      state.title = title;
    },
    setCardTitleColumn: function(state, titleCol) {
      state.cardTitleColumn = titleCol;
    },
    setCustomColName: function(state,payload){
      state.customColNames[payload.title] = payload.name;
    },
    setDisplayType: function(state, type) {
      state.displayType = type;
    },
    setCardType: function(state, type) {
      state.cardType = type;
    } 
  },
  getters:{
    selectedView: state => state.selectedView,
    dataOptions: state => state.dataOptions,
    title: state => state.title,
    cardType: state => state.cardType,
    displayType: state => state.displayType,
    columns: state => state.columns,
    selectedColumns: state => state.selectedColumns
  }
});
